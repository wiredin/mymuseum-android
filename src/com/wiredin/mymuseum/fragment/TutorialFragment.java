package com.wiredin.mymuseum.fragment;

import com.actionbarsherlock.app.SherlockFragment;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;
import com.wiredin.mymuseum.PlaceImageAdapter;
import com.wiredin.mymuseum.R;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;	


@SuppressLint("ValidFragment")
public class TutorialFragment extends SherlockFragment {
	
	PlaceImageAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;

    public static final String TAG = "tutorialFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_tutorial_fragment,
                container, false);
        
        this.getSherlockActivity().getSupportActionBar().setTitle("Tutorial");

        mAdapter = new PlaceImageAdapter(this);

        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
        return view;
    }

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mAdapter = null;
		mPager = null;
		mIndicator = null;
	}
	
	

}
