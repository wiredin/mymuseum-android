package com.wiredin.mymuseum;

import com.actionbarsherlock.app.SherlockActivity;

import android.content.Intent;
import android.os.Bundle;
public class SplashScreen extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(5000);
				} catch (InterruptedException e){
					e.printStackTrace();
				}finally{
					
					SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainActivity.class));
					
					finish();
					
				}
			}
		};
		timer.start();
	}

}
