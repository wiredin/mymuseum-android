package com.wiredin.mymuseum;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

public class ScannedWebViewActivity extends Activity {
	
	private WebView webView;
	
	String id;
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_view);
 
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			id = extras.getString("id");
			Log.d("Hadian", "id recieve: " + id);
		}
		
		webView = (WebView) findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl("http://mymuseum.my/i/"+id);
 
	}

}
