package com.wiredin.mymuseum;

import com.viewpagerindicator.IconPagerAdapter;
import com.wiredin.mymuseum.fragment.PlaceTutorialFragment;
import com.wiredin.mymuseum.fragment.TutorialFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PlaceImageAdapter extends FragmentPagerAdapter implements
		IconPagerAdapter {

	private int[] Images = new int[] { R.drawable.tut1, R.drawable.tut2,
			R.drawable.tut3

	};

	protected static final int[] ICONS = new int[] { R.drawable.circle,
			R.drawable.circle, R.drawable.circle };

	private int mCount = Images.length;

	public PlaceImageAdapter(android.support.v4.app.Fragment fragment) {
		super(fragment.getChildFragmentManager());
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public int getIconResId(int index) {
		// TODO Auto-generated method stub
		return ICONS[index % ICONS.length];
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		return new PlaceTutorialFragment(Images[position]);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mCount;
	}

}
