package com.wiredin.mymuseum;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.wiredin.mymuseum.fragment.AboutFragment;
import com.wiredin.mymuseum.fragment.ScanFragment;
import com.wiredin.mymuseum.fragment.TutorialFragment;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class MainActivity extends SherlockFragmentActivity {

	private ViewPager mPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);

		Tab tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_tab_tutorials)
				.setTabListener(
						new TabListener<TutorialFragment>(this, "tutorial",
								TutorialFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_tab_scan)
				.setTabListener(
						new TabListener<ScanFragment>(this, "scan",
								ScanFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setIcon(R.drawable.ic_tab_about)
				.setTabListener(
						new TabListener<AboutFragment>(this, "about",
								AboutFragment.class));
		actionBar.addTab(tab);

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// MenuInflater inflater = getSupportMenuInflater();
	// inflater.inflate(R.menu.profile, menu);
	//
	// // menu.add("Setting")
	// // .setIcon(R.drawable.ic_action_overflow)
	// // .setShowAsAction(
	// // MenuItem.SHOW_AS_ACTION_IF_ROOM
	// // | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
	// //
	// return true;
	// }

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // TODO Auto-generated method stub
	// switch (item.getItemId()) {
	// case R.id.profile:
	//
	// // Toast.makeText(MainActivity.this, "function 1 called",
	// // Toast.LENGTH_SHORT).show();
	// this.startActivity(new Intent(this, EditProfileActivity.class));
	// break;
	//
	// case R.id.logout:
	//
	// dbHandler.deleteUser();
	// this.startActivity(new Intent(this, MainLayoutActivity.class));
	// this.finish();
	// break;
	//
	// case R.id.cancel:
	//
	// closeOptionsMenu();
	// break;
	// }
	//
	// // String menu = item.getTitle().toString();
	// //
	// // if (menu.equals("Setting")) {
	// // openOptionsMenu();
	// // Toast.makeText(MainActivity.this, "Jadi jadi jadi",
	// // Toast.LENGTH_SHORT).show();
	// // }
	//
	// return true;
	// }

}
